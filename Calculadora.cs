﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculadoraTest
{
    public class Calculadora
    {
        public int Sumar(int a, int b)
        {
            //return 0; // Hace que la prueba falle

            return a + b;
        }

        public int Restar(int a, int b)
        {
            //return 0; 

            return a - b;
        }

        public int Multiplicar(int a, int b)
        {
            //return 0; 

            return a * b;
        }

        public int Dividir(int a, int b)
        {
            //return 0;

            if (b == 0)
            {
                throw new ArgumentException("No se puede dividir por cero");
            }
            return a / b;
        }
    }
}
